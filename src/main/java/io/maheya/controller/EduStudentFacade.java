
package io.maheya.controller;

import io.meheya.model.EduStudent;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author mhernandezy
 */
@Stateless
public class EduStudentFacade extends AbstractFacade<EduStudent> {

    @PersistenceContext(unitName = "PU_registration")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EduStudentFacade() {
        super(EduStudent.class);
    }
    
}
