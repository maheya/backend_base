
package io.maheya.rest;

import io.maheya.controller.EduStudentFacade;
import io.meheya.model.EduStudent;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author mhernandezy
 */


@Path("students")
@RequestScoped
@Produces("application/json")
@Consumes("application/json")

public class EdustudentEndPoint {
    
    
    @EJB
    EduStudentFacade eduStudentService;
    
    
    
    @GET
    public List<EduStudent> listAll(){
        System.out.println("ya hice el get");
        
    return eduStudentService.findAll();
    
    }
    
    
     @GET
     @Path("{id}")
     public EduStudent listAllporid(@PathParam("id") int id){
        
     return eduStudentService.find(id);
    
    }
    
    
    
    @POST
    public Response crear(EduStudent estudiante){
    
        System.out.println("ya entre al POST");
        eduStudentService.create(estudiante);
        return Response.created(UriBuilder.fromResource(EdustudentEndPoint.class).path(String.valueOf(estudiante.getIdStudent())).build()).build();
    
    }
    
    
    @PUT
    @Path("{id}")
    public Response actualizar(@PathParam("id") int id, EduStudent estudiante){

            eduStudentService.edit(estudiante);
            System.out.println("Estudiante modificadoooo:" + estudiante);      
            return Response.noContent().build();
       
    }
    
    
    @DELETE
    @Path("{id}")
    public Response eliminar(@PathParam("id") int id){
        
     EduStudent estudiante = eduStudentService.find(id);
     eduStudentService.remove(estudiante);
     return Response.noContent().build();
    
    }
    
    
    
    
}
